fun main(args: Array<String>) {
    val classSingleton1 = ClassSingleton.instance
    println(classSingleton1.info)
    val classSingleton2 = ClassSingleton.instance
    classSingleton2.info = "Esto es otro singleton"
    println(classSingleton2.info)

    val objectSingleton1 = ObjectSingleton
    println(objectSingleton1.info)
    val objectSingleton2 = ObjectSingleton
    objectSingleton2.info = "Esto es un objeto modificado"
    println(objectSingleton2.info)

}

class ClassSingleton {
    var info = "Esto es un singleton"

    companion object {
        private var INSTANCE: ClassSingleton? = null

        val instance: ClassSingleton
            get() {
                if (INSTANCE == null) {
                    INSTANCE = ClassSingleton()
                }

                return INSTANCE!!
            }
    }
}

object ObjectSingleton {
    var info = "Esto es un objeto inicial"
}